import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * 配置一个空的路由指向home
 * pathMatch:路径匹配方式
 */
const routes: Routes = [
  {
    path:"",
    redirectTo:"/home",
    pathMatch:"full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
