import { Injectable } from "@angular/core";
import { Resolve } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { Banner, HotTag, SonSheet, Singer } from 'src/app/services/data-types/common.types';
import { HomeService } from 'src/app/services/home/home.service';
import { SingerService } from 'src/app/services/singer/singer.service';
import { first } from 'rxjs/internal/operators';
/**
 * 路由守卫
 * 防止组件中加载数据由于数据准备不及时而造成的页面部分地方渲染不出来
 * 在dom渲染器准备好数据，使dom能够正常的渲染
 */
//定义一个Home页面中需要使用到的一个数据
type HomeDataType  = [Banner[],HotTag[],SonSheet[],Singer[]];
@Injectable()
export class HomeResolverService implements Resolve<HomeDataType> {
    /**
     * 构造函数注入了两个服务
     * @param homeServer 
     * @param singerServer 
     */
  constructor(private homeServer: HomeService , private singerServer: SingerService) {}
    /**
     * 守卫方法
     */
  resolve(): Observable<HomeDataType> {
      /**
       * 之前是写了两个请求，现在放在forkjoin里面一起写：首先是简化了一些代码，
       * 其次是能等到一起响应完在进行操作。
       * 就是所谓的：能够实现多异步请求后再执行某方法。
       * first 操作符----只发出可观测源发出的第一个值
       */
    return forkJoin([
        this.homeServer.gitBanner(),
        this.homeServer.getHotTages(),
        this.homeServer.getPersonalSheetlist(),
        this.singerServer.getEnterSinger(),
    ]).pipe(first());
  }
}