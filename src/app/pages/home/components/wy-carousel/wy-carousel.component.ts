import { Component, OnInit, ViewChild, TemplateRef, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-wy-carousel',
  templateUrl: './wy-carousel.component.html',
  styleUrls: ['./wy-carousel.component.less'],
  //changeDetection变更检测策略
  //设置为OnPush时只有输入属性改变的时候才会进行变更检测有利与提高系统性能
  //设计上我们定义的简单组件建议将检测策略改为OnOush
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WyCarouselComponent implements OnInit {
  //@Input()代表输入属性
  @Input()activeIndex:number = 0;
  // @Output定义输出事件
  @Output() chengeslide =  new EventEmitter<'pre'|'next'>();
  //使用模版变量
  @ViewChild('dot', { static: true }) dotRef: TemplateRef<any>;
  constructor() { }

  ngOnInit() {
  }
  //事件绑定的方法
  //type:'pre'|'next'表示是联合类型
  onChengeSlide(type:'pre'|'next'){
    //将按钮传入的type发射出去
    this.chengeslide.emit(type);
  }
}
