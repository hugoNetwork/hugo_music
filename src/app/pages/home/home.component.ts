import { Component, OnInit, ViewChild } from '@angular/core';
import { HomeService } from 'src/app/services/home/home.service';
import { Banner, HotTag, SonSheet, Singer } from 'src/app/services/data-types/common.types';
import { NzCarouselComponent } from 'ng-zorro-antd';
import { SingerService } from 'src/app/services/singer/singer.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/internal/operators';
import { SheetService } from 'src/app/services/sheet/sheet.service';
import { SongService } from 'src/app/services/song/song.service';
/**
 * 发现组件
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  //@ViewChild用来获取组件视图中的元素
  @ViewChild(NzCarouselComponent,{static:true}) private nzCarousel: NzCarouselComponent;
  //接收轮播图改变
  carouselActiveIndex = 0;
  //创建一个变量来接收轮播图数据
  banners:Banner[];
  //创建一个变量来接收热门标签
  tags:HotTag[];
   //创建一个变量来接收热门歌单
   sheetslist:SonSheet[];
   //创建一个变量用来存放入住歌手
   enterSingers: Singer[];
   //导入路由
  constructor(private route: ActivatedRoute,
    private sheetService: SheetService,
    //private homeServer: HomeService , private singerServer: SingerService
    ) { 
      //获取路由守卫返回的数据
      this.route.data.pipe(map(res => res.homeDatas)).subscribe(([banners,tags,sheetslist,enterSingers])=>{
        //给组件中赋值
        this.banners = banners;
        this.tags =  tags;
        this.sheetslist = sheetslist;
        this.enterSingers = enterSingers;
      });
    /*this.gitBanner();
    this.getHotTages();
    this.getPersonalSheetlist();
    this.getEnterSinger();*/
  }
  //获取轮播图
  /*gitBanner(){
    this.homeServer.gitBanner().subscribe((banners)=>{
      //将获取都到的数据给到变量
      this.banners = banners;
      console.log("banners:",banners);
    });
  }
  //获取热门标签
  getHotTages(){
    this.homeServer.getHotTages().subscribe((tags)=>{
      //将获取都到的数据给到变量
      this.tags = tags;
      console.log("tags:",tags);
    });
  }
  //获取热门歌单
  getPersonalSheetlist(){
    this.homeServer.getPersonalSheetlist().subscribe((sheetslist)=>{
      //将获取都到的数据给到变量
      this.sheetslist = sheetslist;
      console.log("sheets:",sheetslist);
    });
  }
  //获取入住歌手
  getEnterSinger(){
    this.singerServer.getEnterSinger().subscribe((singers)=>{
      this.enterSingers =  singers;
      console.log("enterSingers:",singers)
    });
  }
  */

  ngOnInit() {
  }
  //改变监听事件
  //使用条件接收只接受to
  nzBeforeChange({to}){
    //等于改变后的
    this.carouselActiveIndex =  to;
  }
  //页面事件触发
  onChengeSlide(type:'pre'|'next'){
    //直接调用组件的方法
    this.nzCarousel[type]();
  }
  //歌单点击的方法
  onPlaySheet(id: number){
    console.log("id",id);
    //调用服务获取歌单信息
    this.sheetService.playSheet(id).subscribe(res=>{
      console.log("sheetlist",res)
    });
  }
}
