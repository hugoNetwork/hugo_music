import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { HomeResolverService } from './home-resolve.service';

/**
 * 配置路由
 * path:漏油地址
 * component:加载的组件
 * data携带的数据
 */
const routes: Routes = [
  {
    path:"home",
    component:HomeComponent,
    data:{
      title:"发现"
    },
    //为路由添加守卫
    resolve:{
      homeDatas: HomeResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[
    //home的路由守卫
    HomeResolverService
  ]
})
export class HomeRoutingModule { }
