import { Injectable, Inject } from '@angular/core';
import { ServicesModule, API_CONFIG } from '../services.module';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { Banner, HotTag , SonSheet} from '../data-types/common.types';
/**
 * home服务
 * 创建的服务默认放在app模块下
 * 这里我们将服务放在了ServicesModule模块中
 */
@Injectable({
  providedIn: ServicesModule
})
export class HomeService {
  /**
   * 构造函数实现
   * @Inject(A) a:A // 通过依赖注入
   * @param http //在构造函数中注入HttpClient进行网络请求
   * @param url  全局的根路径
   */
  constructor(private http: HttpClient, @Inject(API_CONFIG) private url: string) { }
  /**
   * 返回轮播图的数据
   * map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值。
   * map() 方法按照原始数组元素顺序依次处理元素。
   * 返回一个Observable可观察者对象
   */
  gitBanner(): Observable<Banner[]> {
    return this.http.get(this.url + "/banner").pipe(
      map((res: { banners: Banner[] }) => res.banners)
    );
  }
  //获取热门标签
  //使用sort进行排序
  //并且使用slice进行数据的截取
  getHotTages(): Observable<HotTag[]> {
    return this.http.get(this.url + "/playlist/hot").pipe(
      map((res: { tags: HotTag[] }) => {
        return res.tags.sort((x:HotTag,y:HotTag)=>{
          return x.position - y.position;
        }).slice(0,5);
      })
    );
  }
  //获取热门歌单
  //.slice(0,16)进行数据的截取，获取前16哥数据
  getPersonalSheetlist():Observable<SonSheet[]> {
    return this.http.get(this.url + "/personalized").pipe(
      map((res: { result: SonSheet[] }) => res.result.slice(0,16))
    );
  }
}
