/**
 * 导出轮播图
 */
export type Banner = {
    targetId: number,
    targetType: number,
    imageUrl: string,
    url: string
}
/**
 * 热门标签
 */
export type HotTag = {
    id: number,
    name: string,
    position: number,
}
/**
 * 歌手信息
 */
export type Singer = {
    id: number,
    name: string,
    picUrl: string,
    musicSize: number,
}

/**
 * 歌曲信息
 */
export type Song = {
    id: number,
    name: string,
    url: string,
    ar: Singer[],//歌手
    al: {//专辑
        id: number,
        name: string,
        picUrl: string,
    }
    dt: number //时间
}
/**
 * 歌单
 */
export type SonSheet = {
    id: number,
    name: string,
    picUrl: string,
    playCount: number,
    tracks:Song[]
}
/**
 * 播放地址
 */
export type SongUrl = {
    id: number,
    url: string,
}