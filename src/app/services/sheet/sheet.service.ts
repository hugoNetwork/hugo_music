import { Injectable, Inject } from '@angular/core';
import { ServicesModule, API_CONFIG } from '../services.module';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, pluck, switchMap } from 'rxjs/internal/operators';
import { SonSheet, Song } from '../data-types/common.types';
import { SongService } from '../song/song.service';

@Injectable({
  providedIn: ServicesModule
})
export class SheetService {

  constructor(
    private http:HttpClient ,
     @Inject(API_CONFIG) private url:string,
     private songService: SongService
     ) { }
  //获取歌单详情
  getSongSgeetDetaill(id: number):Observable<SonSheet>{
    //参数
    const params = new HttpParams().set("id",id.toString());
    return this.http.get(this.url+"/playlist/detail",{ params }).pipe(
      map((res: {playlist: SonSheet})=> res.playlist)
    );
   }
   //播放歌单
   playSheet(id: number): Observable<Song[]>{
     /**
      * pluck()筛选其中的某个字段
      * switchMap取消上个订阅转向新的订阅
      */
      return this.getSongSgeetDetaill(id).pipe(pluck('tracks'),switchMap(tracks => this.songService.getSongList(tracks)));
   }
}
