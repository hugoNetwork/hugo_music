import { Injectable, Inject } from '@angular/core';
import { ServicesModule, API_CONFIG } from '../services.module';
import { Observable } from 'rxjs';
import { Singer } from '../data-types/common.types';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators';
//序列化库
import queryString from "query-string";
/**
 * 接口参数
 */
type SingerParams = {
  offset:number,
  limit:number,
  cat?:string
}
/**
 * 默认参数
 */
const defaultParams: SingerParams = {
  offset:0,
  limit:9,
  cat: '5001'
}
/**
 * 歌手服务
 */
@Injectable({
  providedIn: ServicesModule
})
export class SingerService {

  constructor(private http:HttpClient , @Inject(API_CONFIG) private url:string) { }
  /**
   * 获取歌手列表
   * @param args 请求参数
   * args: SingerParams = defaultParams g额默认参数
   */
  getEnterSinger(args: SingerParams = defaultParams):Observable<Singer[]>{
    //创建一个参数
    const params = new HttpParams({fromString: queryString.stringify(args)});
    return this.http.get(this.url+"/artist/list", { params }).pipe(
      map((res: {artists: Singer[]}) => res.artists)
    );
  }
}
