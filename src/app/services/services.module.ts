import { NgModule, InjectionToken } from '@angular/core';
/**
 * 构建一个ng的令牌,
 * 使用令牌
 */
export const API_CONFIG = new  InjectionToken('apiConfig');
/**
 * 服务模块
 */
@NgModule({
  declarations: [],
  imports: [
  ],
  //在Angular中使用依赖注入（DI）的时候，我们一般会使用providers
  providers:[
    //注入一个提供者,配置地址
    {provide:API_CONFIG,useValue:"http://129.28.192.228:3000"}
  ]
})
export class ServicesModule { }
