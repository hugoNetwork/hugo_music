import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_CONFIG, ServicesModule } from '../services.module';
import { Observable, observable } from 'rxjs';
import { SongUrl, Song } from '../data-types/common.types';
import { map } from 'rxjs/internal/operators';

@Injectable({
  providedIn: ServicesModule
})
export class SongService {

  constructor(private http:HttpClient , @Inject(API_CONFIG) private url:string) { }
  //获取歌曲信息
  getSongUrl(ids: string):Observable<SongUrl[]>{
    //参数
    const params = new HttpParams().set("id",ids);
    return this.http.get(this.url+"/song/url",{ params }).pipe(
      map((res: { data: SongUrl[]}) => res.data)
    );
  }
  //获取歌曲集合
  getSongList(songs: Song | Song[]):Observable<Song[]>{
    //参数处理,始终是一个数组
    const songArr = Array.isArray(songs)? songs.slice() : [songs];
    //参数转换
    //遍历获取id并用,链接
    //join()链接操作
    const ids = songArr.map(item => item.id).join(",")
    //调用服务获取数据
    /**
     * 由于generateSongList方法返回的仅仅是一个普通的song数组
     * 我们的getSongList需要返回一个Observable的可观察对象
     * 我们需要对我们的数据进行封装
     *  使用Observable.create()创建一个颗观察对象create方法接收一个observable对象
     * observable.next()将数据提供给外部订阅者,是订阅者可以拿到数据
     */
    /*return Observable.create(observable =>{
      this.getSongUrl(ids).subscribe(urls => {
        observable.next(this.generateSongList(songArr, urls));
      });
    });*/
    //返回observable的简化写法
    return this.getSongUrl(ids).pipe(map(urls => this.generateSongList(songArr,urls)));
  }
  //歌曲数据拼接
  private generateSongList(songs: Song [] , urls: SongUrl[]):Song[]{
    //创建数组
    const result  = [];
    //循环歌曲列表
    songs.forEach(song =>{
      //在歌曲urls集合中找id和歌曲id相同的数据，并获取url
      const url = urls.find(url => url.id === song.id).url;
      //url存在
      if(url){
        //将歌曲保存到数组中
        //{...song,url}将song打散并将url加入到对象中
        result.push({...song,url})
      }
    });
    return result;
  }
}
