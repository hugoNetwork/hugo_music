import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'playCount'
})
//
/**
 * 播放量处理管道
 */
export class PlayCountPipe implements PipeTransform {
  /**
   * 字符处理
   * @param value 传入的值
   */
  transform(value:number): number | string {
    if(value >10000){
      return Math.floor(value / 10000) + "万"
    }else{
      return value;
    }
  }

}
