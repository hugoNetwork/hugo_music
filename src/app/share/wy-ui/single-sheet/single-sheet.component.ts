import { Component, OnInit, Input, ChangeDetectionStrategy, Output , EventEmitter } from '@angular/core';
import { SonSheet } from 'src/app/services/data-types/common.types';

@Component({
  selector: 'app-single-sheet',
  templateUrl: './single-sheet.component.html',
  styleUrls: ['./single-sheet.component.less'],
  //changeDetection变更检测策略
  //设置为OnPush时只有输入属性改变的时候才会进行变更检测有利与提高系统性能
  //设计上我们定义的简单组件建议将检测策略改为OnOush
  changeDetection:ChangeDetectionStrategy.OnPush
})
/**
 * 歌单组件
 */
export class SingleSheetComponent implements OnInit {
  //输入属性
  @Input() sheet: SonSheet;
  //输出属性
  @Output() onPlay =  new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }
  //歌单点击
  playSheet(id: number){
    //触发事件
    this.onPlay.emit(id);
  }
}
