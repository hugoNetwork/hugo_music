import { Observable } from 'rxjs'

export type WySliderStyle = {
    width?: string | null;
    height?: string | null;
    left?: string | null;
    bottom?:  string | null;
}
//事件类型声明
export type SliderEventObserverCongig = {
    //鼠标按下
    start: string;
    //鼠标移动
    move: string;
    //鼠标抬起
    end: string;
    //筛选事件,判断是否是鼠标事件
    filter: (e: Event) => boolean;
    //定义获取当前位置的数组
    pluckkey: string [];
    startPlucked$?: Observable<number>;
    moveResolved$?: Observable<number>;
    end$?: Observable<Event>;
}