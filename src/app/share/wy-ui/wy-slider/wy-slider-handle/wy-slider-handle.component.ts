import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { WySliderStyle } from '../wy-slider-type';

@Component({
  selector: 'app-wy-slider-handle',
  template: '<div class="wy-slider-handle" [ngStyle]="style"></div>',
  ////改变变更检测策略，当输入变化是改变
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WySliderHandleComponent implements OnInit,OnChanges {
  //是否垂直
  @Input() wyVertical = false;
  //偏移量
  @Input() wyLength: number;
  //样式
  style: WySliderStyle = {}
  constructor() { }

  ngOnInit() {
  }
 //生命周期函数
  //ngOnchanges在父组件（初始化或修改）子组件的输入参数下调用
  ngOnChanges(changes: SimpleChanges): void {
    //用来监听偏移量变化
    if(changes['wyLength']){
      //判断是垂直还是水平
      if(this.wyVertical){
        this.style.height =  this.wyLength + "%";
        this.style.left = null;
        this.style.width = null;
      }else{
        this.style.width = this.wyLength + "%";
        this.style.bottom = null;
        this.style.height = null;
      }
    }
  }

}
