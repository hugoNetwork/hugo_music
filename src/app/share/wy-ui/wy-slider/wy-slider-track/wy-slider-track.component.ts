import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { WySliderStyle } from '../wy-slider-type';

@Component({
  selector: 'app-wy-slider-track',
  //定义一个内部的模板并绑定style属性
  template: '<div class="wy-slider-track" [ngStyle]="style"></div>',
  //改变变更检测策略，当输入变化是改变
  changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * 按钮组件
 */
//ngOnchanges在父组件（初始化或修改）子组件的输入参数下调用
export class WySliderTrackComponent implements OnInit,OnChanges {
  //是否垂直
  @Input() wyVertical = false;
  //偏移量
  @Input() wyOffset: number;
  //样式
  style: WySliderStyle = {}
  constructor() { }

  ngOnInit() {
  }
  //生命周期函数
  //ngOnchanges在父组件（初始化或修改）子组件的输入参数下调用
  ngOnChanges(changes: SimpleChanges): void {
    //用来监听偏移量变化
    if(changes['wyOffset']){
      //判断是垂直还是水平
      this.style[this.wyVertical ? 'bottom' : 'left'] = this.wyOffset + '%';
    }
  }

}
