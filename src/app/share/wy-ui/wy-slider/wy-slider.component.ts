import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ViewChild, ElementRef, Input, Inject } from '@angular/core';
import { fromEvent, Observable, merge } from 'rxjs';
import { tap, pluck, map, distinctUntilChanged, takeUntil, filter } from 'rxjs/internal/operators';
import { SliderEventObserverCongig } from './wy-slider-type';
import { DOCUMENT } from '@angular/common';
import { sliderEvent } from './wy-slider-helper';

@Component({
  selector: 'app-wy-slider',
  templateUrl: './wy-slider.component.html',
  styleUrls: ['./wy-slider.component.less'],
  //视图封装模式，默认只在组件内部生效，改为None则代表全局的样式在全局生效
  encapsulation: ViewEncapsulation.None,
  ////改变变更检测策略，当输入变化是改变
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WySliderComponent implements OnInit {
  //定义一个输入参数获取是否垂直
  @Input() wyVertical = false;
  //总体的观察对象
  private dragStart$: Observable<number>
  private dragMove$: Observable<number>
  private dragEnd$: Observable<Event>
  //定义一个事件的容器
  private sliderDom: HTMLDivElement;
  //获取模板中的dom
  @ViewChild('wySlider', { static: true }) private wySlider: ElementRef
  constructor(@Inject(DOCUMENT) private doc: Document) { }
  //初始化的生命周期函数
  ngOnInit() {
    //给容器对象赋值
    this.sliderDom = this.wySlider.nativeElement;
    this.createDraggingObservables();
  }
  //给滑块添加事件
  private createDraggingObservables() {
    /**
     * 获取鼠标的位置
     * pc：
     *  event.pageX 水平
     *  event.pageY 垂直
     * 移动端：
     *  event.touchsp[0].pageX 水平
     *  event.touchsp[0].pageY 垂直
     */
    //判断是否垂直来获取鼠标的位置
    const orientField = this.wyVertical ? 'pageY' : 'pageX';
    //定义pc端绑定的事件
    const mouse: SliderEventObserverCongig = {
      //鼠标按下
      start: "mousedown",
      //鼠标移动
      move: "mousemove",
      //鼠标抬起
      end: "mouseup",
      //筛选事件,判断是否是鼠标事件
      filter: (e: MouseEvent) => e instanceof MouseEvent,
      //定义获取当前位置的数组
      pluckkey: [orientField]
    };
    //定义手机端事件
    const touch: SliderEventObserverCongig = {
      //手指进入
      start: "touchstart",
      //移动
      move: "touchmove",
      //抬起
      end: "touchend",
      //事件筛选，判断是否是触摸事件
      filter: (e: TouchEvent) => e instanceof TouchEvent,
      //定义当前触摸的位置
      pluckkey: ['touches', '0', orientField]
    };
    //遍历需要绑定的事件
    [mouse, touch].forEach(source => {
      //获取事件的值
      //filter: filterFunc重命名
      const { start, move, end, filter: filterFunc, pluckkey } = source;
      //将一个元素上的事件转化为一个Observable
      //接收两个参数，第一个是事件容器，第二个是事件
      //filter 自定义过滤规则，符合才把该值进行发射
      //tap ( 窃听 ) 是两个完全相同的操作符，用于窃听Observable的生命周期事件，而不会产生打扰。
      //pluck是map的简化用法。
      source.startPlucked$ = fromEvent(this.sliderDom, start).pipe(filter(filterFunc),
        tap(sliderEvent),
        pluck(...pluckkey),
        map((position: number) => this.findClosestValue(position)));
      //绑定end事件
      source.end$ = fromEvent(this.doc, end);
      //绑定move事件
      source.moveResolved$ = fromEvent(this.doc, move).pipe(
        filter(filterFunc), tap(sliderEvent), pluck(...pluckkey),
        //当值发生改变的时候继续向下发射流
        distinctUntilChanged(),
        map((position: number) => this.findClosestValue(position)),
        //表示在end事件的时候结束
        takeUntil(source.end$)
      );
    });
    //使用merge操作符合并
    this.dragStart$ = merge(mouse.startPlucked$, touch.startPlucked$);
    this.dragMove$ = merge(mouse.moveResolved$, touch.moveResolved$);
    this.dragEnd$ = merge(mouse.end$, touch.end$);
  }
}
